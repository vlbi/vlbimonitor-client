What's New
==========

Changes since v2:
-----------------
- Use https over standing TCP connection.
- Submit the client code version in HTTP header.
- Support multiple privacy modes: public, private, ...
- Use low-overhead server API
- Support both python2 and python3

Changes since v1:
-----------------
- Cache data records for asynchronous submission.
- Automatically resubmit records after connection issues.
- On record rejection filter schema-violating content, print warnings, and resubmit.
- Use a dedicated parameter.map file instead of complicated masterlist customization.
- Parameters with a cadance value of 0 are interpreted as 'state parameter', meaning that every change in value will be submitted immediately.  Other parameters are interpreted as "measurements parameters".  Measurements will be rate-limited to cadence and submission is possibly deferred a little bit.
- Treat data points that are not yet due for submission as 'pending'.
- Automatically re-read and parse masterlist and parameter.map files.
- Submission is rate limited to <=1 per second.
- Submit heartbeat packet after HEARTBEAT seconds of no activity.
- Catch and properly shutdown client and all modules on SIGINT and SIGTERM signals.
- Write data records to file instead of complete JSON-RPC requests.

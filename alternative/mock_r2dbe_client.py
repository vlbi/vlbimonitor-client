#!/usr/bin/env python2
from __future__ import print_function

import requests
import getpass
import random
import json
import time

import numpy as np

serverurl = "https://casdev.science.ru.nl/local"

def get_8bit_states(s=32):
    x = np.arange(-128,128)
    y = 262144*np.exp(-(x**2)/(2*s**2)) / (s*np.sqrt(2*np.pi))
    return [int(yy) for yy in y]

def get_8bit_spectrum(spur=0,rolloff=6):
    n = 2048
    x = np.arange(0,n)
    y = np.random.normal(loc=0,scale=0.2,size=(n,)) - rolloff*x/n
    y[1024] = y[1024] + spur
    y = y - max(y)
    return [float(yy) for yy in y]

def get_r2dbe_data():
    return [
        { # R2DBE 1
            'EightBitStateCounts0': get_8bit_states(s=32),
            'EightBitStateCounts1': get_8bit_states(s=1),
            'TwoBitStateCounts0': [random.randint(1, 3) for _ in range(4)],
            'TwoBitStateCounts1': [random.randint(1, 3) for _ in range(4)],
            'EightBitSpectrum0': get_8bit_spectrum(spur=0,rolloff=6),
            'EightBitSpectrum1': get_8bit_spectrum(spur=100,rolloff=3),
        },
        {}, #R2DBE 2, 
        { # R2DBE 3
            'EightBitStateCounts0': get_8bit_states(s=18),
            'EightBitStateCounts1': get_8bit_states(s=23),
            'TwoBitStateCounts0': [random.randint(1, 3) for _ in range(4)],
            'TwoBitStateCounts1': [random.randint(1, 3) for _ in range(4)],
            'EightBitSpectrum0': get_8bit_spectrum(spur=0,rolloff=1),
            'EightBitSpectrum1': get_8bit_spectrum(spur=0,rolloff=2),
        },
        { # R2DBE 4
            'EightBitStateCounts0': get_8bit_states(s=42),
            'EightBitStateCounts1': get_8bit_states(s=37),
            'TwoBitStateCounts0': [random.randint(1, 3) for _ in range(4)],
            'TwoBitStateCounts1': [random.randint(1, 3) for _ in range(4)],
            'EightBitSpectrum0': get_8bit_spectrum(spur=2,rolloff=4),
            'EightBitSpectrum1': get_8bit_spectrum(spur=1,rolloff=3),

    }]

def post_r2dbe_data(data, username, password):
    r = requests.post(serverurl, data=json.dumps(data), auth=(username, password), timeout=4)
    print(r)

def main():
    username = input('Username: ')
    password = getpass.getpass('Password for {}: '.format(username))
    t1 = time.time()
    while True:
        data = get_r2dbe_data()
        post_r2dbe_data(data, username, password)
        t1 += 4
        time.sleep(t1 - time.time())

if __name__ == "__main__":
    main()

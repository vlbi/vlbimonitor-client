# This is a VLBImonitor client.  This client is released under the terms of the MIT Licence.
# Copyright (c) 2016 Radboud RadioLab.  All rights reserved

#-- standard libraries
import datetime
import glob
import os
import queue
import random
import resource
import socket
import struct
import threading
import time

import logging
logger = logging.getLogger(__name__)
logging.getLogger("becs").setLevel(logging.WARN)

#-- add-on libraries
import redis

#-- from vlbicontrol
import becs.bdc
import becs.config
import becs.mark6
import becs.r2dbe
import config
import r2daemon
import vdif
import vexinterpreter
import vexparser

backend_config_file = "/etc/backend.conf"

#-- helper functions
#-- limit significant figures{{{
def signif(n, x):
    """Print float to specified number of significant figures"""
    return float("%.*g" % (n, x))

def nsignif(x):
    '''Return number of significant figures'''
    return len(str(x).split('e')[0].replace(".", ""))

def ndecimal(x):
    '''Return the number of decimals in the input'''
    words = str(x).split(".")
    if len(words) < 2: return 0
    return len(words[1])
#}}}

class Sitedata(object):
    """Implements site-dependent data acquisition."""
    def __init__(self, sitelist):
        self.sitelist = sitelist
        #-- generate fake acquirers
        self.acquirers = [
          MockR2DBEData("r2dbe1", station_0="Sz", pol_0="lcp", rx_sb_0="lsb", bdc_ch_0="high", station_1="Sz", pol_1="rcp", rx_sb_1="lsb", bdc_ch_1="high"),
          MockR2DBEData("r2dbe3", station_0="Sz", pol_0="lcp", rx_sb_0="usb", bdc_ch_0="low", station_1="Sz", pol_1="rcp", rx_sb_1="usb", bdc_ch_1="low"),
          MockR2DBEData("r2dbe4", station_0="Sz", pol_0="lcp", rx_sb_0="usb", bdc_ch_0="high", station_1="Sz", pol_1="rcp", rx_sb_1="usb", bdc_ch_1="high"),
          MockDualBandBDCData("bdc2"),
          MockMark6Data("recorder2"),
          MockMark6Data("recorder3"),
          MockMark6Data("recorder4"),
        ]

    def close(self, signum, frame):
        """Clean up this class, e.g. close any threads used"""
        logger.debug("Sitedata received signal %d, closing...", signum)

    def collect(self):
        """Collect data points"""
        #-- start with empty dict
        params = {}

        #-- collect from self
        now = time.time()
        #rsrc = resource.getrusage(resource.RUSAGE_SELF)
        #params["cc1.GBEClientUtime"] = [[now, rsrc[0]]]
        #params["cc1.GBEClientStime"] = [[now, rsrc[1]]]
        #params["cc1.GBEClientMaxrss"] = [[now, rsrc[2]]]

        #-- collect from acquirers in separate threads
        acq_thds = [threading.Thread(target=acq.collect_and_update, args=(params,)) for acq in self.acquirers]
        [at.start() for at in acq_thds]
        [at.join() for at in acq_thds]

        return params

class Getter(Sitedata):
    """Forwards compatibility interface to Sitedata.

    Getter is deprecated, use Sitedata instead."""
    def __init__(self, sitelist):#{{{
        super(Getter, self).__init__(sitelist)
        self.params = {}
#}}}

class MockR2DBEData(object):
    """Implements R2DBE-specific data acquisition."""
    def __init__(self, host, **kwargs):#{{{
        """Instantiate an R2DBE-data acquisition object.

        Arguments:
        name -- full device name in config file
        config -- vlbicontrol/config.Config object from config file
        """
        self.host = host
        #-- randomise few fixed parameters (one per device)
        self.fixed_params = {
          "status": ["unreachable","unconfigured","configured"][random.randint(0,2)],
          "up_since": int(time.time()) - random.randint(3600,15*24*3600),
          "pps_offset": random.randint(-64,63),
          "serialNumber": "02440102" + hex(random.randint(0,2**16-1))[-4:],
        }
        #-- randomise more fixed parameters (one per channel)
        for ch in [0,1]:
            self.fixed_params["time_sec_offset%d" % ch] = random.randint(-3,3)
            self.fixed_params["pol_%d" % ch] = ["lcp","rcp"][random.randint(0,1)]
            self.fixed_params["rx_sb_%d" % ch] = ["lsb","usb"][random.randint(0,1)]
            self.fixed_params["bdc_ch_%d" % ch] = ["low","high"][random.randint(0,1)]
            self.fixed_params["station_%d" % ch] = ["Aa","Bb","Cc"][random.randint(0,2)]
            self.fixed_params["2bit_th_%d" % ch] = random.randint(0,100)
        #-- accept any fixed parameters given by creator
        for k,v in kwargs.items():
            self.fixed_params[k] = v

    def _dynamic_params(self):
        dynamic_params = {
          "uptime": int(time.time()) - self.fixed_params["up_since"],
        }
        return dynamic_params

    def collect_and_update(self, export_params):
        """Collect data points and add to given parameter dictionary"""
        params = {}

        now = time.time()

        #-- get fixed params
        for k,v in self.fixed_params.items():
            params[k] = [[now, v]]

        #-- get dynamic params
        for k,v in self._dynamic_params().items():
            params[k] = [[now, v]]

        #-- update export parameters
        export_params.update(self._prefix_params(params))

    def _prefix_params(self, params_in):
        params_out = {}
        for k, v in list(params_in.items()):
            params_out[".".join((self.host,k))] = params_in[k]
        return params_out

#}}}

class MockSingleBandBDCData(object):
    """Implements BDC-specific data acquisition."""
    def __init__(self, host, **kwargs):#{{{
        """Instantiate an Single-band BDC data acquisition object.

        Arguments:
        name -- full device name in config file
        config -- vlbicontrol/config.Config object from config file
        """
        self.host = host
        #-- randomise few fixed parameters (one per device)
        self.fixed_params = {
          "status": ["unreachable","unconfigured","configured"][random.randint(0,2)],
          "serial_number": "SN%03d" % random.randint(0,999),
          "hardware_version": "2.0",
          "firmware_version": "3.3",
          "configuration": "Single-Band",
          "control_status": ["Front-panel/Remote", "Front-panel", "Remote"][random.randint(0,2)],
        }
        #-- randomise more fixed parameters (one per input channel)
        for ch in ["A", "B"]:
            self.fixed_params["lo_freq_%s" % ch] = random.randint(6000,6001)
            self.fixed_params["lo_freq_%s_ref" % ch] = random.randint(6000,6001)
            self.fixed_params["lo_status_%s" % ch] = ["LOCKED","UNLOCKED","OFF","FAULT"][random.randint(0,3)]
            # and then per output channel
            for i in [0, 1]:
                for j in ["L", "U"]:
                    self.fixed_params["attn_%s%d%s" % (ch,i,j)] = random.randint(0,63)/2
        #-- accept any fixed parameters given by creator
        for k,v in kwargs.items():
            self.fixed_params[k] = v

    def _dynamic_params(self):
        dynamic_params = {}
        return dynamic_params

    def collect_and_update(self, export_params):
        """Collect data points and add to given parameter dictionary"""
        params = {}

        now = time.time()

        #-- get fixed params
        for k,v in self.fixed_params.items():
            params[k] = [[now, v]]

        #-- get dynamic params
        for k,v in self._dynamic_params().items():
            params[k] = [[now, v]]

        #-- update export parameters
        export_params.update(self._prefix_params(params))

    def _prefix_params(self, params_in):
        params_out = {}
        for k, v in list(params_in.items()):
            params_out[".".join((self.host,k))] = params_in[k]
        return params_out
#}}}

class MockDualBandBDCData(object):
    """Implements BDC-specific data acquisition."""
    def __init__(self, host, **kwargs):#{{{
        """Instantiate an Dual-band BDC data acquisition object.

        Arguments:
        name -- full device name in config file
        config -- vlbicontrol/config.Config object from config file
        """
        self.host = host
        #-- randomise few fixed parameters (one per device)
        self.fixed_params = {
          "status": ["unreachable","unconfigured","configured"][random.randint(0,2)],
          "serial_number": "SN%03d" % random.randint(0,999),
          "hardware_version": "2.0",
          "firmware_version": "3.3",
          "configuration": "Dual-Band",
          "control_status": ["Front-panel/Remote", "Front-panel", "Remote"][random.randint(0,2)],
          "band": ["4-8","5-9"][random.randint(0,1)]
        }
        #-- randomise more fixed parameters (one per input channel)
        for ch in ["4to8", "5to9"]:
            self.fixed_params["lo_freq_%s" % ch] = random.randint(6000,6001)
            self.fixed_params["lo_freq_%s_ref" % ch] = random.randint(6000,6001)
            self.fixed_params["lo_status_%s" % ch] = ["LOCKED","UNLOCKED","OFF","FAULT"][random.randint(0,3)]
        # and then per output channel
        for i in [0, 1]:
            for j in ["L", "U"]:
                self.fixed_params["attn_P%d%s" % (i,j)] = random.randint(0,63)/2
        #-- accept any fixed parameters given by creator
        for k,v in kwargs.items():
            self.fixed_params[k] = v

    def _dynamic_params(self):
        dynamic_params = {}
        return dynamic_params

    def collect_and_update(self, export_params):
        """Collect data points and add to given parameter dictionary"""
        params = {}

        now = time.time()

        #-- get fixed params
        for k,v in self.fixed_params.items():
            params[k] = [[now, v]]

        #-- get dynamic params
        for k,v in self._dynamic_params().items():
            params[k] = [[now, v]]

        #-- update export parameters
        export_params.update(self._prefix_params(params))

    def _prefix_params(self, params_in):
        params_out = {}
        for k, v in list(params_in.items()):
            params_out[".".join((self.host,k))] = params_in[k]
        return params_out
#}}}

class MockMark6Data(object):
    """Implements Mark6-specific data acquisition"""
    def __init__(self, host, **kwargs):#{{{
        """Instantiate a Mark6 data acquisition object.

        Arguments:
        name -- full device name in config file
        config -- vlbicontrol/config.Config object from config file
        """
        self.host = host
        #-- randomise few fixed parameters (one per device)
        self.fixed_params = {
          "status": ["unreachable","unconfigured","configured"][random.randint(0,2)],
          #"m6ccStatus": ["not running"],
          #"ShouldRecord": False,
          #"DPlaneTalking": random.randint(0,3) > 0,
          "scancheckId": "e99x42_Xx_123-1234",
          "scancheckDatarate":  16448 - random.randint(0,500),
          "scancheckBytesMissing": random.randint(0,1024),
          "scancheckStatus": ["OK","data?","time?"][random.randint(0,2)],
        }
        #-- randomise more fixed parameters (per module slot)
        for mod in [1,2,3,4]:
            self.fixed_params["module%dID" % mod] = "XYZ%%%d" % (1230+mod)
        #-- randomise more fixed parameters (per group)
        for g in ["b", "d"]:
            self.fixed_params["group%sSpooling" % g] = False
            self.fixed_params["group%sModules" % g] = "12" if g == "b" else "34"
            self.fixed_params["group%sChainlabel" % g] = "lcp_lsb_low"
            self.fixed_params["group%sDataRate" % g] = 8.224
            self.fixed_params["group%sSpaceLeft" % g] = random.randint(1,3)*2*3600
            self.fixed_params["group%sFillRate" % g] = random.randint(0,200)*1e-6
        #-- accept any fixed parameters given by creator
        for k,v in kwargs.items():
            self.fixed_params[k] = v

    def _dynamic_params(self):
        dynamic_params = {}
        return dynamic_params

    def collect_and_update(self, export_params):
        """Collect data points and add to given parameter dictionary"""
        params = {}

        now = time.time()

        #-- get fixed params
        for k,v in self.fixed_params.items():
            params[k] = [[now, v]]

        #-- get dynamic params
        for k,v in self._dynamic_params().items():
            params[k] = [[now, v]]

        #-- update export parameters
        export_params.update(self._prefix_params(params))

    def _prefix_params(self, params_in):
        params_out = {}
        for k, v in list(params_in.items()):
            params_out[".".join((self.host,k))] = params_in[k]
        return params_out
#}}}

#}}}
# vim: foldmethod=marker

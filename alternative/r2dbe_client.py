#!/usr/bin/env python3

import requests
import getpass
import random
import json
import time

import numpy as np
import redis

import config
import becs.config
import becs.r2dbe.data as r2data
import r2daemon.r2daemon as r2daem

username = ""
password = ""
serverurl = "http://localhost:8000/local"

redis_server = redis.Redis()

def get_r2dbe_data(cfg):
    def redis_key(dev, k):
        return bytes(ord(c) for c in (dev + ".raw." + k))
    def get_value(name):
        param_dict = r2data.lookup_param(name)
        binary = r2daem.decode(redis_server[redis_key(param_dict["dev"])])
        return param_dict["decode"](binary)
    r2dbe_data = []
    for host in ["r2dbe%d" % i for i in range(1,5)]:
        result = {}
        try:
            dev = [k for k,v in list(cfg.devices.items()) if "hostname" in v.keys() and v["hostname"] == host][0]
        except (KeyError, IndexError):
            # could not find R2DBE with hostname
            r2dbe_data.append(result)
            continue
        try:
            last_update = float(redis_server[redis_key(dev, "_last_update")])
            if time.time() - last_update > 5:
                # add empty result for stale data
                r2dbe_data.append(result)
                continue
        except KeyError:
            # add empty result for missing R2DBEs
            r2dbe_data.append(result)
            continue
        for ch in [0, 1]:
            # 2-bit and 8-bit spectra & 2-bit counts
            for b in [2, 8]:
                x = range(2048)
                unpack = getattr(r2data, "unpack_%dbit_data" % b)
                samples = np.array(unpack(r2daem.decode(redis_server[redis_key(dev, "r2dbe_snap_%dbit_%d_data_bram" % (b, ch))])))
                if b == 8:
                    y = np.fft.fft(samples.reshape((-1, len(x)*2)), axis=-1)
                    y = abs((y * y.conj()).mean(axis=0))[:len(x)]
                    y = 10*np.log10(y/max(y))
                    if any(np.isnan(y)) or any(np.isinf(y)):
                        y = np.ones_like(y)
                    result["%sBitSpectrum%d" % ({2:"Two", 8:"Eight"}[b], ch)] = y.tolist()
                if b == 2:
                    x = range(-2,2)
                    result["TwoBitStateCounts%d" % ch] = [len(samples[samples == i]) for i in x]
            # 8-bit counts
            counts_dict = r2data.unpack_8bit_counts(r2daem.decode(redis_server[redis_key(dev, "r2dbe_monitor%d_counts" % ch)]))
            # sum over 4 ADC cores
            counts = np.array(counts_dict["counts"]).sum(axis=1)
            # check for -1 entries which mean incomplete count
            result["EightBitStateCounts%d" % ch] = np.array(counts[-1,:]).tolist() if not any(counts[-1,:] == -1) else np.array(counts[-2,:]).tolist()
        r2dbe_data.append(result)
    return r2dbe_data

def post_r2dbe_data(data):
    #print(json.dumps(data))
    requests.post(serverurl, data=json.dumps(data), auth=(username, password), timeout=4)

def main():
    cfg = config.Config("/etc/backend.conf")
    t1 = time.time()
    while True:
        data = get_r2dbe_data(cfg)
        post_r2dbe_data(data)
        t1 += 2
        t_sleep = t1 - time.time()
        if t_sleep > 0: time.sleep(t_sleep)

main()

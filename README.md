VLBImonitor Client
==================
This is a client for the VLBImonitor software.  It submits VLBI metadata (status information) from a telescope site to the [VLBImonitor server](https://bitbucket.org/vlbi/vlbimonitor-server).

License
-------
This client is available under the terms of the [MIT License](http://opensource.org/licenses/MIT).


Design
------
A client communicates with the VLBImonitor server using JSON records over HTTPS.  Data is submitted as an array of [measurement-time, value] pairs per parameter.  An example JSON record is shown below.

Parameter names in a data record are verified against a white list on the server.  The white listed parameters are defined in `masterlist.csv`.

The Masterlist also defines distinct reporting modes for the client in which different sets of parameters will be reported.  The mode 'public' limits the client to parameters that are privacy insensitive, such as weather and maser.  The reporting mode is selected real-time by writing the mode name into the `privacy_mode` file.


Minimalistic Client
-------------------
`minimal.py` is a minimalistic example client that demonstrates submitting the example record below.


Reference Client
----------------
The reference client separates generic functionality (e.g. construction and submission of JSON records) from site-specific functionality (e.g. data collection).

1. generic
	- `client.py`: driver routine
	- `vlbimon`: interface with server
	- `masterlist.csv`: parameter definitions

2. site-specific
	- `sitedata` creates a dictionary with data from the site
	- `parameter.map` defines a mapping from sitedata dictionary key names to the Formal Parameter Names in `masterlist.csv`.


Dependencies
------------
1. [python-requests](http://python-requests.org)


Getting started
---------------

0. Install dependencies

1. Clone the client repo

		# git clone https://bitbucket.org/vlbi/vlbimonitor-client.git
		# cd vlbimonitor-client

2. Setup the `sitedata` module.  Replace SMA with your station code in vlbimon/config.py.

		# cp sitedata/example.py sitedata/SMA.py
		# echo 'from .SMA import *' > sitedata/__init__.py

	This module is site-specific.  example.py is only used as a reference here.

3. Create the `parameter.map` definitions file

		# tools/gen-parameter.map masterlist.csv > sitedata/SMA.map
		# ln -sf sitedata/SMA.map parameter.map
		# vimdiff parameter.map sitedata/example.map

	Note that there are two columns:

		1. formal parameter name (matches masterlist.csv)
		2. site's parameter name

	Parameters that are not mandatory are double commented out by default

3. Set your client's authentication credentials in `vlbimon/config.py`:

		# cp vlbimon/config-sample.py vlbimon/config.py
		# vim vlbimon/config.py

	These can be requested from the [maintainer](mailto:d.vanrossum@astro.ru.nl)


Upgrading from a Previous Version
---------------------------------
See the UPGRADE.md file.


Example Data Record
-------------------
Parameter values are timestamped with the time at which the value was **originally measured** at the site (units: seconds since 1970-01-01 UTC), not the time at which the value is collected by the client!

	{
		"telescope_onSource": [[1534756757, true]],
		"if_1_systemTemp": [[1534756758, 315]]
	}

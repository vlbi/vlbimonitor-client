# This file is part of the VLBImonitor client.  This client is released under the terms of the MIT Licence.
# Copyright (c) 2016 Radboud RadioLab.  All rights reserved
from __future__ import print_function

import time
import re
import os
import subprocess
import socket
import threading
import calendar
import logging
#-- fix threading bug in time module:
import _strptime

#TODO
#-- discriminate between recorder cplaneOnline and schedulerOnline

class Recorder():
    """Describe a host that runs a job scheduler"""
    SCHEDULERCMD = "(M6_CC|m6cc.py)"
    RECORDER_WATCHPID_INTERVAL = 6  #-- poll interval in seconds for active schedulers
    def __init__(self, hostname):#{{{
        self.hostname = hostname

        self.permit = TimedToggle()
        #-- assume that we startup outside a recording window
        self.permit.set_then(0)  #-- set now

        #-- ssh state
        self.reachable = True  #-- warn if not reachable

        #-- cplane
        self.cplane = Cplane(hostname, self.permit)

        self.logs = []  #-- collect log objects

        self.pid = 0
        #-- helpers
        self.pids_current = []  #-- assume inactive
        # [-1]: inaccessible
        # []: inactive
        # [x]: active
        # [x,y]: multi-process problem

        #-- open a socket and reconnect when it fails
        self.pidthread = RetryConditioned(self.RECORDER_WATCHPID_INTERVAL, self.permit, self.__find_logs)
        self.pidthread.start()

    def close(self):
        self.permit.close()#{{{
        self.pidthread.cancel()
        self.cplane.close()
        for log in self.logs:
            log.close()
        self.pidthread.join()
#}}}

    def collect(self):
        t = int(time.time())#{{{

        #-- only obtain information when permitted
        if not self.permit.is_set(): return {'recording': [[t, True]], 'status': [[t, 'recording']]}
        self.cplane.read_scaninfo()
        #-- only obtain information when still permitted
        if not self.permit.is_set(): return {'recording': [[t, True]], 'status': [[t, 'recording']]}

        #-- we're not recording, apparently
        params = {}
        params['recording'] = [[t, False]]

        #print('collecting info', time.strftime('%Yy%jd%Hh%Mm%Ss', time.gmtime()))

        #-- pidthread finds out if reachable.  read_shellcmd is blocking...
        if self.reachable:
            params_ = self.read_shellcmd()
            for k,v in params_.items():
                if k not in params: params[k] = []
                params[k].append(v)

        if self.pid > 0:
            params['online'] = [[t, True]]
            # status comes from the log below
        else:
            params['online'] = [[t, False]]
            params['status'] = [[t, 'not scheduling']]

        #-- read logs
        params['logFilename'] = [[t, '-']] #-- default
        for log in self.logs:
            params_ = self.read_log(log)
            for k,v in params_.items():
                if k not in params: params[k] = []
                params[k] += v

            #-- insert online and log filename, overwriting the previous value
            params['logFilename'] = [[t, log.filename]]

        if not self.reachable:
            params['status'] = [[t, 'unreachable']]

        return params
#}}}

    def read_log(self, log):
        """Read info from log and start permit timers if scanStartTime is found"""
        params = log.read_tail()  #always returns a dict#{{{

        if 'scanStartTime' in params  and  'scanDuration' in params:
            t0 = params['scanStartTime'][-1][1]
            dt = int(params['scanDuration'][-1][1])
            t0 = time.strptime(t0, '%Y%j%H%M%S')
            t0 = calendar.timegm(t0)
            t1 = t0 + dt
            self.permit.set_timers(t0, t1)

        #-- mark old log inactive after reading
        if log.pid != self.pid:
            log.pid = 0

        return params
#}}}

    def read_shellcmd(self):
        """Obtain status information through shell commands"""
#{{{
        def read_df():
            remotecmd = 'df -txfs /dev/sd[^a]1 /dev/sda[a-z]1 || true' #{{{
            cmd = ['ssh', '-q', '-o ConnectTimeout 1', self.hostname, remotecmd]
            p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            out, err = p.communicate()
            returncode = p.wait()

            if returncode != 0: return {}

            if 'No such file or directory' in err  or  'no file systems processed' in err:
                params = {}
                params['diskfreeNofDisks'] = [tnow, 0]
                params['diskfreeNofModules'] = [tnow, 0]
                return params
            elif err != '':
                logging.warning('unexpected df stderr: %s', err.strip())
                return {}

            lines = out.strip()

            #-- put results in dict
            params = {}
            nofdisk, usedt, availt = 0, 0, 0
            modules = set()
            for line in lines.split("\n")[1:]:
                words = line.split()
                #-- skip non-table lines
                if len(words) != 6: continue

                devid, size, used, avail, fill, mountpoint = words

                #-- skip disks that are not mounted
                s = '/mnt/disks'
                if mountpoint[:len(s)] != s: continue

                #-- count modules
                s = '/mnt/disks/.'
                s = mountpoint[0:len(s)]
                modules.add(s)

                #-- totals
                nofdisk += 1
                usedt += int(used)
                availt += int(avail)

            if nofdisk > 0:
                y = float(usedt)/1024**3
                params['diskfreeUsed'] = [tnow, float("%.3f" % y)]
                y = float(availt)/1024**3
                params['diskfreeFree'] = [tnow, float("%.3f" % y)]
                y = 100*usedt/float(usedt + availt)  #-- unit percent
                params['diskfreeFillfactor'] = [tnow, float("%.2f" % y)]

            params['diskfreeNofDisks'] = [tnow, nofdisk]
            params['diskfreeNofModules'] = [tnow, len(modules)]

            return params
#}}}

        tnow = int(time.time())
        #-- execute remote command
        params = read_df()

        return params
#}}}

    def __find_logs(self):
        """scan for running processes and take action if changes are detected"""
        #-- clean up old logs  #{{{
        self.logs = [log for log in self.logs if log.pid != 0]

        #-- pids that are currently followed
        pids = [log.pid for log in self.logs]

        #-- find new pid
        pid = self.__find_pids()

        if pid is None: return  # no result
        if pid in pids: return  # already being followed

        if self.pid > 0:
            logging.error('M6_CC status(%s): unfollow pid %d', self.hostname, self.pid)

        #-- adopt
        self.pid = pid
        if pid <= 0: return

        logging.error('M6_CC status(%s): follow pid: %d', self.hostname, pid)

        #-- trigger filename update
        logfilename = self.__read_logfilename(pid)
        if logfilename is None: return

        #-- instantiate new object
        log = Schedulerlog(self.hostname, logfilename, pid)
        self.logs.append(log)
#}}}

#}}}

    def __find_pids(self):
        """Check for running schedulers on the Mark6 and capture their process IDs.
        There should only be one instance at a time."""
        remotecmd = 'pgrep "^{:}$" || true'.format(self.SCHEDULERCMD) #{{{
        cmd = ['ssh', '-q', '-o ConnectTimeout 1', self.hostname, remotecmd]
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = p.communicate()
        returncode = p.wait()

        if returncode != 0:
            if self.reachable:
                logging.error('M6_CC status(%s): access lost: %s', self.hostname, err.strip())
            self.reachable = False
            return
        if not self.reachable:
            logging.error('M6_CC status(%s): access restored', self.hostname)
        self.reachable = True

        pids = out.strip().split()
        pids = [int(s) for s in pids]

        #-- handle errors
        if len(pids) == 0:
            #-- nothing changed
            if self.pids_current != pids:
                logging.error('M6_CC status(%s): no instance running', self.hostname)
                self.pids_current = pids
            return 0

        #-- multiple instances
        if len(pids) > 1:
            if self.pids_current != pids:
                logging.error('M6_CC status(%s): multiple instances: %s', self.hostname, str(pids))
                self.pids_current = pids
            if self.pid in pids:
                return self.pid
            return -1

        #-- single instance
        if len(self.pids_current) > 1:
            logging.error('M6_CC status(%s): resolved multi-instance issue', self.hostname)
        self.pids_current = pids

        return pids[0]
#}}}

    def __read_logfilename(self, pid):
        """Read the log filename from the current proc's open file descriptors."""
        #-- The log file handle is the first after stdin/stdout/sterr, i.e. number 3 #{{{
        remotecmd = 'readlink -f /proc/{:}/fd/3 || true'.format(pid)
        cmd = ['ssh', '-q', self.hostname, remotecmd]
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = p.communicate()
        returncode = p.wait()

        if returncode != 0: return

        fname = out.strip()

        if fname == '': return

        if len(fname) < 4  or  fname[-4:] != '.log':
            logging.error('M6_CC status(%s): log filename format error: does not end with ".log": %s', self.hostname, fname)
            return

        logging.error('M6_CC status(%s): log filename: %s', self.hostname, fname)
        return fname
#}}}
#}}}

class Schedulerlog():
    """Scheduler log file class"""
    SCHEDULERLOG_FILEPOSITION_LOG = os.path.dirname(__file__) + '/recorder_schedulerlog_fileposition.log'
    def __init__(self, hostname, filename, pid):#{{{
        self.hostname = hostname
        self.filename = filename
        self.pid = pid

        #-- logfile line counter
        self.ibytes = self.load_fileposition()

    def close(self):
        self.save_fileposition()

    def load_fileposition(self):
        """Read previous file position from file"""
        try:#{{{
            with open(self.SCHEDULERLOG_FILEPOSITION_LOG, 'r') as f:
                lines = f.readlines()
        except IOError:
            return 0

        ibytes = 0
        for line in lines:
            #-- empty line
            if len(line) == 0: continue
            #-- comment line
            if line[0] == "#": continue
            words = line.strip().split('\t')
            #-- incorrect line
            if len(words) != 3: continue
            timestamp_, filename_, ibytes_ = words
            #-- does not apply
            if filename_ != self.filename: continue
            #-- overwrite previous matches
            ibytes = int(ibytes_)

        if ibytes != 0:
            logging.info('restore log fileposition: %s, %d', self.filename, ibytes)

        return ibytes
#}}}

    def save_fileposition(self):
        """Read current file position to file"""
        with open(self.SCHEDULERLOG_FILEPOSITION_LOG, 'a') as f:#{{{
            f.write("%d\t%s\t%d\n" % (int(time.time()), self.filename, self.ibytes))
#}}}

    def read_tail(self):
        """Read new lines in log file and return a (possibly empty) dict"""
#{{{
        #-- mark log as inactive
        remotecmd = 'tail -c +{:} {:}'.format(self.ibytes+1, self.filename)
        cmd = ['ssh', '-q', self.hostname, remotecmd]
        try:
            lines = subprocess.check_output(cmd)
        except subprocess.CalledProcessError:
            return {}

        self.ibytes += len(lines)

        if len(lines) == 0: return {}
        logging.debug('log position (bytes): %d', self.ibytes)

        params = {}
        for line in lines.strip().split('\n'):
            params_ = self.__parse_line(line)
            for k,v in params_.items():
                if k not in params:
                    params[k] = []
                #-- skip duplicate
                elif params[k][-1] == v:
                    continue
                params[k].append(v)

        params['hostname'] = [[int(time.time()), self.hostname]]

        logging.debug('read_tail: params: %s', str(params))

        return params
#}}}

    def __parse_line(self, line):
        """M6_CC logfile line parser"""
        phrases = line.strip().split(' - ') #{{{
        #-- skip silently:
        if len(phrases) != 5: return {}

        timestamp = phrases[0]
        loglevel = phrases[2]
        info = phrases[4]

        #-- no timestamp
        if len(timestamp) != 23: return {}
        #-- no info
        if len(info) == 0: return {}

        #-- time
        #x = '{:}T{:}.{:}Z'.format(line[:10], line[12:19], line[21:23])
        #x = dateutil.parser.parse(timestamp)
        #x = calendar.timegm(x.timetuple())
        try:
            x = time.strptime(timestamp, '%Y-%m-%d %H:%M:%S,%f')
        except ValueError:
            logging.warning('parse_line: timestamp error, skipping: %s', line)
            return {}
        x = calendar.timegm(x)

        #-- loglevel
        if loglevel == "WARNING":
            if info == "skipping this scan...trying the next": return {}
            return {'execWarningmsg': [x, info]}
        if loglevel == "ERROR":
            return {'execErrormsg': [x, info]}

        #-- info
        params = {}

        match = re.match("^Parsed input schedule file ([\w\/]+\.xml)$", info)
        if match is not None:
            params['scheduleFilename'] = [x, match.group(1)]
            return params

        match = re.match("^still have input fn ([\w\/]+\.xml)$", info)
        if match is not None:
            params['scheduleFilename'] = [x, match.group(1)]
            return params

        match = re.match("^(\w+\.xml) file processing complete$", info)
        if match is not None:
            params['status'] = [x, "end of schedule"]
            return params

        match = re.match('^source (\w+) starting (\d+) dur (\d+) scan_name (\S+)$', info)
        if match is not None:
            params['scanSource'] = [x, match.group(1)]
            params['scanStartTime'] = [x, match.group(2)]
            params['scanDuration'] = [x, int(match.group(3))]
            params['scanName'] = [x, match.group(4)]
            #try:
            #    y = time.strptime(match.group(2), '%Y%j%H%M%S')
            #except ValueError:
            #    pass
            #else:
            #    params['scanStartDay'] = [x, time.strftime('%Y-%m-%d', y)]
            #    params['scanStartTime'] = [x, time.strftime('%M:%H:$S', y)]
            return params

        match = re.match('^scan (\d+) of (\d+)$', info)
        if match is not None:
            params['scanNumber'] = [x, int(match.group(1))]
            params['scanTotalnumber'] = [x, int(match.group(2))]
            return params

        match = re.match('^mk6 ret: !(\w+[=?])(.*);$', info)
        if match is not None:
            words = match.group(2).split(':')
            #-- input_stream
            SLEN = 9  #-- number of words per stream
            if match.group(1) == 'input_stream?':
                words = words[2:]
                nstream = len(words) / SLEN
                s = [':'.join([words[i*SLEN], words[i*SLEN + 5]]) for i in range(nstream)]
                params['inputstreamLabels'] = [x, ', '.join(s)]
                params['inputstreamCount'] = [x, len(s)]
                return params
            #-- record
            if match.group(1) == 'record=':
                params['status'] = [x, 'pending']
                return params
            if match.group(1) == 'record?':
                params['status'] = [x, words[2]]
                params['scanName'] = [x, words[-1]]
                return params
            #-- rtime
            if match.group(1) == 'rtime?':
                params['groupDatarate'] = [x, int(float(words[3]))]
                params['groupTimeLeft'] = [x, int(float(words[4]))]
                params['groupSpaceLeft'] = [x, int(words[5])]
                params['groupSpaceTotal'] = [x, int(words[6])]
                return params
            #-- scan_check
            if match.group(1) == 'scan_check?':
                params['scancheckId'] = [x, words[4]]
                #if words[5] != 'unk':
                if len(words) >= 14:
                    params['scancheckStatus'] = [x, words[7]]
                    params['scancheckDatarate'] = [x, int(float(words[12]))]
                    params['scancheckBytesMissing'] = [x, int(words[13])]
                    return params

        #logging.debug('line ignored: %s', line)
        return {}
#}}}
#}}}


class Cplane():
    """Talk to Mark6 cplane via socket.  The main purpose is to track
    when a Mark6 permits monitoring activity.  It does so by reading
    scan time information and setting permit toggle timers based on that."""
    SOCKET_RECONNECT_INTERVAL = 5  #-- socket reconnect interval in seconds
    CPLANE_PORT = 14242 #-- cplane port
    def __init__(self, hostname, permit):#{{{
        self.permit = permit
        self.hostname = hostname
        self.socket = None

        #-- track which errors were already printed
        self.sockiniterrs = set()

        self.last_timestamp = None

        self.readthread = RetryConditioned(self.SOCKET_RECONNECT_INTERVAL, self.permit, self.read_scaninfo)
        self.readthread.start()

    def close(self):
        """Close the threads started in this class"""
        if self.socket is not None: self.socket.close()#{{{
        self.readthread.cancel()
        self.readthread.join()
#}}}

    def read_scaninfo(self):
        """Read the next scan start/end time and set permit toggle timers.
        TODO: mutex this routine."""
        cmd = 'scan_info?;'#{{{
        res = self.query1(cmd)
        #!scan_info?0:0:34:989:test_RU_109-0006:pending:2018y109d00h06m44s:12:1:0

        if res is None: return
        if len(res) == 0: return

        words = res.split(':')
        if len(words) < 8:
            logging.warning('unexpected reply from cplane: %s: %s', self.hostname, res)
            return

        status, timestamp, duration = words[5:8]

        if timestamp == '-': return
        if duration == 'unk': return

        #-- no new information
        if timestamp == self.last_timestamp: return
        self.last_timestamp = timestamp

        t0 = time.strptime(timestamp, '%Yy%jd%Hh%Mm%Ss')
        t0 = calendar.timegm(t0)
        t1 = t0 + int(duration)

        #-- set permit window timers
        self.permit.set_timers(t0, t1)

        return res
#}}}

    def query1(self, cmd):
        """Send one command and read result"""
        try:#{{{
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.settimeout(1)
            s.connect((self.hostname, self.CPLANE_PORT))
        except socket.error as e:
            #-- print once
            if str(e) not in self.sockiniterrs:
                logging.warning('socket init error: %s; %s: %s', self.hostname, type(e).__name__, str(e))
            s.close()
            self.sockiniterrs.add(str(e))
            return
        #-- reset errors on success
        if len(self.sockiniterrs) != 0:
            logging.warning('socket init success: %s', self.hostname)
            self.sockiniterrs = set()

        res = None
        try:
            cmd = cmd.strip().rstrip(';') + ';'
            s.sendall(cmd)
            res = s.recv(8192).strip()
        except socket.error as e:
            logging.warning('socket comm error: %s; %s: %s', self.hostname, type(e).__name__, str(e))

        s.close()
        return res
#}}}

#}}}


class RetryConditioned(threading.Thread):
    """Run function on interval if permit state allows it"""
    def __init__(self, interval, permit, function, exit_on_success=False, args=[], kwargs={}):#{{{
        threading.Thread.__init__(self)
        self.interval = interval
        self.function = function
        self.exit_on_success = exit_on_success
        self.args = args
        self.kwargs = kwargs
        self.permit = permit
        self.finished = threading.Event()

    def cancel(self):
        """Stop the timer if it hasn't finished yet"""
        self.finished.set()

    def run(self):
        while not self.finished.is_set():
            if self.permit.is_set():
                ret = self.function(*self.args, **self.kwargs)
                if self.exit_on_success and ret is None: self.finished.set()
            self.finished.wait(self.interval)
#}}}


class PerpetualTimer(threading.Thread):
    """Run a function at a set time"""
    def __init__(self, func):#{{{
        threading.Thread.__init__(self)
        self.func = func
        self.interval = 0
        self.timerReset = threading.Event()
        self.finished = threading.Event()

    def set_then(self, t):
        tnow = time.time()
        self.interval = max(0, t - tnow)
        logging.debug('set %s dt=%.2f, t=%.0f, now=%.0f', self.__name__, self.interval, t, tnow)
        self.timerReset.set()

    def cancel(self):
        """Interrupt the timer loop"""
        self.finished.set()
        self.timerReset.set()

    def run(self):
        while not self.finished.is_set():
            #-- wait for input
            self.timerReset.wait()
            #-- now set timer
            self.timerReset.clear()
            if self.finished.is_set(): return
            timer_reset = self.timerReset.wait(self.interval)
            if timer_reset:
                logging.debug('reset \'%s\' timer', self.func.__name__)
                pass
            elif not self.finished.is_set():
                logging.debug('run function \'%s\'', self.func.__name__)
                self.func()
#}}}


class TimedToggle(threading._Event):
    """A timed state toggle"""
#{{{
    def __init__(self):
        super(TimedToggle, self).__init__()
        self.t_setlast = 0
        self.onTimer = PerpetualTimer(self.set)
        self.onTimer.__name__ = 'on-timer'
        self.onTimer.start()
        self.offTimer = PerpetualTimer(self.clear)
        self.offTimer.__name__ = 'off-timer'
        self.offTimer.start()

    def close(self):
        self.onTimer.cancel()
        self.offTimer.cancel()
        self.onTimer.join(1)
        self.offTimer.join(1)

    def set_then(self, t):
        self.t_setlast = t
        self.onTimer.set_then(t)

    def clear_then(self, t):
        #-- don't permit setting a clear timer without setting the followup set timer first
        assert t < self.t_setlast
        self.offTimer.set_then(t)

    def set_timers(self, t0, t1):
        #-- sanity check{{{
        tnow = time.time()
        if t1 <= tnow:
            logging.debug('skipping timer to be set in the past: tend=%.0f, tnow=%.0f', t1, time.time())
            return

        #-- set on-timer before off-timer
        self.set_then(t1 + 1)
        #-- immediate effect
        if t0 - 1 <= tnow:
            self.clear()
        else:
            self.clear_then(t0 - 1)
#}}}
#}}}

# vim: foldmethod=marker

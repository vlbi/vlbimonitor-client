# This is a VLBImonitor client.  This client is released under the terms of the MIT Licence.
# Copyright (c) 2016 Radboud RadioLab.  All rights reserved
from __future__ import print_function

import time
from decimal import Decimal as D
from gltWeather import gltWeather
from ehtMonRedisData import ehtMonRedisData

#-- exact floating point arithmetic
def sumf(a, b):
    return float(D(str(a)) + D(str(b)))
def prodf(a, b):
    return float(D(str(a)) * D(str(b)))


class Sitedata(object):
    def __init__(self, sitelist):
        self.sitelist = sitelist

    def close(self):
        """Clean up this class, e.g. close any threads used"""
        pass

    def collect(self):
        """Collect data points"""
        #-- start with empty dict
        params = {}

        #-- Insert [t, value] data points, where t is the time at
        #-- which the value was originally measured.
        #-- Don't use the 'time' module in production!!
        utime = time.mktime(time.localtime())

        (mode,rAHours,decDegs,source,az,el,tau,tsys1,tsys2)=ehtMonRedisData()
        params['TEL.OBS_MODE'] = [[utime, mode]]
        params['TEL.STATUS'] = [[utime, '']]
        if decDegs < 0.0:
            coordString = '%7.6f%7.6f' % (rAHours, decDegs)
        else:
            coordString= '%7.6f+%7.6f' % (rAHours, decDegs)
        params['TEL.APP_RA_DEC'] = [[utime, coordString]]
        azElString = '%1.5f+%1.5f' % (az, el)
        params['TEL.AZEL'] = [[utime, azElString]]
        params['IF1.CENTER_FREQ'] = [[utime, 347.558]]
#        params['IF1.CENTER_FREQ'] = [[utime, 2.0]]
        params['IF1.TSYS'] = [[utime, float(tsys1)]]
#        params['IF1.TSYS_AZEL'] = [[utime, azElString]]
        params['IF2.CENTER_FREQ'] = [[utime, 347.558]]
#        params['IF2.CENTER_FREQ'] = [[utime, 2.0]]
        params['IF2.TSYS'] = [[utime, float(tsys2)]]
#        params['IF2.TSYS_AZEL'] = [[utime, azElString]]
#        params['BDC1.DC_FREQ'] = [[utime, 7.85]]
        params['TEL.SOURCE_NAME'] = [[utime, source]]
#        params['TEL.EPOCH'] = [[utime, 'J2000']]
#        params['GAIN.STATUS'] = [[utime, False]]
#        params['PHASING.STATUS'] = [[utime, False]]
#        params['PHASING.REF_ANT'] = [[utime, 6]]
#        params['PHASING.REF_PAD'] = [[utime, 'pad 1']]
#        params['PHASING.REF_COORDS'] = [[utime, '+19.82420-155.47752']]
#        params['PHASING.PHASE_CENTER'] = [[utime, coordString]]
#        params['PHASING.N_ANTENNAS'] = [[utime, len(phasedAntennas)]]
        (temperature,humidity,pressure,windspeed,winddirection,windchill)=gltWeather()
        params['ENVIRO.Air_Temp'] = [[utime, sumf(temperature, 273.15)]] #-- convert C to K
        params['ENVIRO.Pressure'] = [[utime, prodf(.1, pressure)]] #-- convert hPa to kPa
        params['ENVIRO.Humidity'] = [[utime, humidity]]
        if humidity > 97.0:
            params['ENVIRO.rain_event'] = [[utime, 'fog']]
        else:
            params['ENVIRO.rain_event'] = [[utime, 'dry']]
        params['ENVIRO.Wind_DIR'] = [[utime, winddirection]]
        params['ENVIRO.Wind_Speed'] = [[utime, windspeed]]
        params['ENVIRO.ONLINE'] = [[utime, True]]
        params['ENVIRO.tau225'] = [[utime, tau]]

        return params

# vim: foldmethod=marker

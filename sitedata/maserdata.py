#!/usr/bin/python2
#-- by Kamal Souccar, 13/03/2017
from __future__ import print_function

import os
from numpy import pi
import csv
from StringIO import StringIO
import time
from datetime import datetime
from pytz import timezone
from lxml import etree
import re
import urllib2

# pass empty dictionary to bypass proxy
proxy = urllib2.ProxyHandler({})
opener = urllib2.build_opener(proxy)
urllib2.install_opener(opener)

# The maser computer is EST
# need to add the difference between EST and my timezone to the maser time
eastern_tz = timezone('US/Eastern')
my_tz = timezone(time.tzname[0])
now_t = datetime.now()
eastern_t = eastern_tz.localize(now_t)
my_t = my_tz.localize(now_t)
dt_tz = (eastern_t-my_t).total_seconds()

# init dict
params = {}
key2key = {
    'Top_Plate_Heater': 'topPlateHeater',
    'AC-DC#1': 'acDc1',
    'AC-DC#2': 'acDc2',
    'IF Amplitude Alarm': 'ifAmplitudeAlarm',
    'AC_1_and_2_Available': 'ac1and2Available',
    'VCO_Lock_Alarm': 'vcoLockAlarm',
    'Outer_Oven_Heater': 'outerOvenHeater',
    'Register_Limit_Alarm': 'registerLimitAlarm',
    'Hydrogen_Pressure': 'hydrogenPressure',
    'VI_Pump_(H2)': 'viPumpH2',
    'Discharge_Voltage': 'dischargeVoltage',
    'DC_EXT._Available': 'dcExtAvailable',
    'Bottle_Heater': 'bottleHeater',
    'Lower_Support_Heater': 'lowerSupportHeater',
    'IF Amplitude': 'ifAmplitude',
    'Thermal_Shield': 'thermalShield',
    'Local_Oscillator': 'localOscillator',
    'Pd._(H2_Valve)_Heater': 'pdH2ValveHeater',
    'Thumbwheel_switches': 'thumbwheelSwitches',
    'VI_Pump_(Upper)': 'viPumpUpper',
    'VCO_Control_Voltage': 'vcoControlVoltage',
    'Cavity_Register': 'cavityRegister',
    'Pirani_Gauge_Heater': 'piraniGaugeHeater',
    'Ext_conf': 'extConf',
    'Ext_Synth': 'extSynth',
    'Averager': 'averager',
    'Battery_In_User_(Alarm)': 'batteryInUseAlarm',
    'Front_panel_switch': 'frontPanelSwitch',
    'Main_Magnetic_Field': 'mainMagneticField',
    'Battery_Voltage': 'batteryVoltage',
    'Main_bus': 'mainBus',
    'VCO_Heater': 'vcoHeater',
    'Discharge_Current': 'dischargeCurrent',
    'DCx_(When_Present)': 'dcx',
    'Battery_Charge': 'batteryCharge',
    'Cavity_Heater': 'cavityHeater'}



def updateValues():
    utime = time.mktime(time.localtime())

    params.clear()

    # get the data from the maser computer
    try:
        response = urllib2.urlopen('http://192.168.2.40/maser/')
        lines = response.readlines()
    except Exception as ex:
        print(ex)
        return

    s = ''
    inTable = 0

    for line in lines:
        if '<p>Updated' in line:
            m = re.search('<p>Updated - (.+?)</p>', line)
            if m:
                mtime =  m.group(1)
                mtime = datetime.strptime(mtime, '%Y%m%d-%H:%M:%S %Z')
                mytime = time.mktime(mtime.timetuple())+dt_tz
                print("maser time: ",utime,mytime,utime-mytime)
                utime = mytime
        if '&' in line:
            line = line.replace('&', "and")
        if '<table>' in line:
            s += line
            inTable = 1
        elif '</table>' in line:
            s += line
            inTable = 0
        elif inTable == 1:
            s += line

    if s == '':
        params['maser.status'] = [utime, False]
        return
    
    table = etree.XML(s)
    rows = iter(table)
    for row in rows:
        entry = [col.text for col in row]
        key = key2key[entry[0]]
        value = entry[1]
        try:
            value = float(value)
        except:
            pass

        params['maser.'+key] = [utime, value]

    params['maser.status'] = [utime, True]


if __name__ == '__main__':
    updateValues()
    print(key2key)

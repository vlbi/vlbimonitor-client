# This is a VLBImonitor client.  This client is released under the terms of the MIT Licence.
# Copyright (c) 2016 Radboud RadioLab.  All rights reserved
from __future__ import print_function

import time
import math
from datetime import datetime
import subprocess
import os
import numpy as np

### PabloT temporal fix to the RA/DEC position:
# Read the paKo catalogue -> Read sourcename from telescopeStatus -> obtain RaDec from pako catalogue
sources = {}
#with open("apr2017_1mm_short.sou") as fff:
#    sourcecatalogue = fff.readlines()

sourcecatalogue = np.genfromtxt("apr2017_1mm_short.sou", comments="!", dtype='S200')
for source in sourcecatalogue:
    #info = source.split()
    #print(info)
    #print(source)
    sources[source[0]] = [source[3], source[4]]

sourcecatalogue = np.genfromtxt("poinwithflux.edb", comments="!", delimiter=",", \
                    usecols=(0, 2, 3), dtype='S200')
for source in sourcecatalogue:
    #info = source.split()
    #print(info)
    #print(source)
    if source[0].rstrip() not in sources.keys(): # i.e. only add from the pointing catalogue the sources that were not present in apr2017_1mm_short.sou
        sources[source[0].rstrip()] = [source[1], source[2]]
    #else:
    #    print("Skipping re-adding %s"%source[0].rstrip())
###


#-- limit significant figures
def signif(n, x):
    """Print float to specified number of significant figures"""
    return float("%.*g" % (n, x))

def nsignif(x):
    '''Number of significant figures'''
    return len(str(x).replace(".", ""))

def ndecimal(x):
    '''Number of decimals'''
    words = str(x).split(".")
    if len(words) < 2: return 0
    return len(words[1])

def parse_minsec(coord):
    words = [float(s) for s in coord.split(":")]
    if len(words) < 3: return 0
    return math.copysign(abs(words[0]) + words[1]/60. + words[2]/60**2, words[0])


class Getter():
    def __init__(self, sitelist):
        self.params = {}
        self.sitelist = sitelist

    def updateValues(self):
        #-- default timestamp: now
        #-- time: seconds since 1970 Jan 1 UTC, also known as "unix time".
        utime = int(time.time())

        #-- start with a clear dict
        self.params.clear()

        self.params["weather.online"] = [utime, False]
        self.params["telescope.status"] = [utime, "offline"]
        self.params["rx.online"] = [utime, False]
        self.params["rxCS.E230.isLocked"] = [utime, False]  # PabloT added

#-- fetch data
        try:
            # Use telescopeStatus (works only from mrt-lx1!) to fetch data
            output = subprocess.check_output('telescopeStatus')
        except Exception as e:
            print(e)
        else:
            #-- output to dict
            data = {}
            for line in output.split("\n"):
                words = line.strip().split()
                # allow more than two words, e.g. in rx.use if two receivers are present
                if len(words) < 2: continue
                par, val = words[0], words[1]
                data[par] = val

            self.insertWeather(utime, data)
            self.insertScanInfo(utime, data)
            self.insertAntMd(utime, data)
            self.insertAntCs(utime, data)
            self.insertRxCs(utime, data)

        #-- fetch Tsys, added by PabloT
        try:
            # Use "getvlbicals1mm" (works only from mrt-lx3) to get Tsys
            getvlbicals1mm = subprocess.check_output('ssh -l hello mrt-lx3 getvlbicals1mm', shell=True)
        except Exception as e:
            print(e)
        else:
            try:
                self.insertTsys(utime, getvlbicals1mm)
            except Exception as e:
                print(e)
                # PabloT is anything fails, send me an email!
                os.system('echo Something happened with the VLBI client of Pico, please check. \n Exception: %s | mail -s "Pico VLBI monitor Info" torne@iram.es'%e )

        #-- fetch isLocked, added by PabloT
        try:
            # Use "superviseReceiver.py" (works only from mrt-lx1) to get the Lock status of the E230
            superviseReceiver = subprocess.check_output('/local/users/torne/software/superviseReceiver.py', shell=True)
        except Exception as e:
            # PabloT is anything fails, send me an email!
            os.system('echo Something happened with the VLBI client of Pico, please check. \n Exception: %s | mail -s "Pico VLBI monitor Info" torne@iram.es'%e )
        else:
            try:
                self.insertRXlock(utime, superviseReceiver)
            except Exception as e:
                print(e)
                # PabloT is anything fails, send me an email!
                os.system('echo Something happened with the VLBI client of Pico, please check. \n Exception: %s | mail -s "Pico VLBI monitor Info" torne@iram.es'%e )

    def insertScanInfo(self, utime, data):
        par = "scanInfo.overview.timeStamp"
        if par in data:
            tstr = data[par]
            tstr = datetime.strptime(tstr, "%Y-%m-%dT%H:%M:%S.%f").strftime("%s")
            t = int(tstr)
        else:
            t = utime

        par = "scanInfo.overview.observingMode"
        if par in data: self.params[par] = [t, data[par]]
        par = "scanInfo.overview.sourceName"
        if par in data:
            srcname = data[par]
            self.params[par] = [t, srcname]
            # PabloT: this line hides the source being observed  (comment when EHT obsverations to show source)
            #self.params["scanInfo.overview.sourceName"] = [t, "n/a"]
            if srcname in list(sources.keys()):
                # PabloT: get the RaDec from the sources dictionary:
                ra = sources[srcname][0].replace(" ","")
                ra = parse_minsec(ra)
                dec = sources[srcname][1].replace(" ","")
                dec = parse_minsec(dec)
                radec = "{:.{ndec}f}{:+.{ndec}f}".format(ra, dec, ndec=7)
                self.params["RaDecfromCatalogue"] = [t, radec]
                self.params["RaDecEpochType"] = [t, "J2000"]
                print(" ** Debug ** self.param['RaDecfromCatalogue'] = %s"%self.params["RaDecfromCatalogue"])
            else:
                print("Current source (%s) not in vlbi catalogue."%self.params["scanInfo.overview.sourceName"][1])


    def insertWeather(self, utime, data):
        self.params['weather.online'] = [utime, True]

        #-- timestamp
        par = "weather.tau.Tstamp"
        if par in data:
            tstr = data[par]
            tstr = datetime.strptime(tstr, "%Y-%m-%dT%H:%M:%S").strftime("%s")
            timeTau = int(tstr)
        else:
            timeTau = utime

        par = "weatherStation.data.timeStamp"
        if par in data:
            tstr = data[par]
            tstr = datetime.strptime(tstr, "%Y-%m-%dT%H:%M:%S.%f").strftime("%s")
            t = int(tstr)
        else:
            t = utime

        par = "weather.tau.tau"
        if par in data: self.params[par] = [timeTau, float(data[par])]

        # PabloT: Following ATM07 model, we can calculate pwv from tau225GHz: tau(225) = 0.058 * pwv + 0.004
        par = "weather.pwv.fromModel"
        self.params[par] = [timeTau, float("%.2f"%((self.params["weather.tau.tau"][1] - 0.004)/0.058))]

        par = "weatherStation.data.temperature"
        if par in data:
            val = data[par]
            self.params[par] = [t, signif(nsignif(val), 273.15 + float(val))]
        par = "weatherStation.data.pressure"
        if par in data: self.params[par] = [t, signif(4, .1 * float(data[par]))]

        pars = ["weatherStation.data.humidity", "weatherStation.data.windVel", "weatherStation.data.windDir"]
        for par in pars:
            if par in data: self.params[par] = [t, float(data[par])]


    def insertAntMd(self, utime, data):
        self.params['telescope.status'] = [utime, "online"]
        #-- timestamp
        par = "antMD.trace.timeStamp"
        if par in data:
            tstr = data[par]
            tstr = datetime.strptime(tstr, "%Y-%m-%dT%H:%M:%S.%f").strftime("%s")
            t = int(tstr)
        else:
            t = utime

        if "antMD.trace.basisLong" in data  and  "antMD.trace.basisLat" in data:
            lon = data["antMD.trace.basisLong"]
            lat = data["antMD.trace.basisLat"]
            ndec = max(list(map(ndecimal, (lon, lat))))
            lonlat = "{:.{ndec}f}{:+.{ndec}f}".format(float(lon), float(lat), ndec=max(ndec, 1))
            self.params["antMD.trace.basisLongLat"] = [t, lonlat]

        if "antMD.trace.actualAz" in data  and  "antMD.trace.actualEl" in data:
            az = data["antMD.trace.actualAz"]
            el = data["antMD.trace.actualEl"]
            ndec = max(list(map(ndecimal, (az, el))))
            az, el = list(map(float, (az, el)))
            if el > 90: el -= 360.
            azel = "{:.{ndec}f}{:+.{ndec}f}".format(float(az), float(el), ndec=max(ndec, 1))
            self.params["antMD.trace.actualAzEl"] = [t, azel]

        if "antMD.trace.xOffset" in data  and  "antMD.trace.yOffset" in data:
            xoff = data["antMD.trace.xOffset"]
            yoff = data["antMD.trace.yOffset"]
            pointing = "/".join([xoff, yoff])
            self.params["antMD.trace.pointingCorrection"] = [t, pointing]


    def insertAntCs(self, utime, data):
        self.params['telescope.status'] = [utime, "online"]
        #-- timestamp
        par = "antCS.focus.set.timeStamp"
        if par in data:
            tstr = data[par]
            tstr = datetime.strptime(tstr, "%Y-%m-%dT%H:%M:%S.%f").strftime("%s")
            t = int(tstr)
        else:
            t = utime

        if "antCS.focus.set.focusCorrectionX" in data  and \
               "antCS.focus.set.focusCorrectionY" in data  and \
               "antCS.focus.set.focusCorrectionZ" in data:
            valx = data["antCS.focus.set.focusCorrectionX"]
            valy = data["antCS.focus.set.focusCorrectionY"]
            valz = data["antCS.focus.set.focusCorrectionZ"]
            self.params["antCS.focus.set.focusCorrection"] = [t, "/".join([valx, valy, valz])]


    def insertRxCs(self, utime, data):
        if data["scanInfo.overview.rx.use"] != "E230": return
        #if data["rxCS.use.receiver"] != "EMIR"  or \
        #        data["rxCS.EMIR.beam.receiver"] != "E230": return
        self.params['rx.online'] = [utime, True]

        #-- timestamp
        par = "rxCS.E230.frequency.timeStamp"
        if par in data:
            tstr = data[par]
            tstr = datetime.strptime(tstr, "%Y-%m-%dT%H:%M:%S.%f").strftime("%s")
            t = int(tstr)
        else:
            t = utime

        pars = [
            "rxCS.E230.frequency.skyFrequency",
            "rxCS.E230.frequency.ambientLoadTemp",
            "rxCS.E230.frequency.coldLoadTemp",
            "rxCS.E230.frequency.centerIF",
            "rxCS.E230.frequency.bandwidth"]
        for par in pars:
            if par in data: self.params[par] = [t, float(data[par])]

        par = "rxCS.E230.frequency.sideBand"
        if par in data: self.params[par] = [t, data[par]]


    def insertTsys(self, utime, data):
        if data == '':
            t = utime
            self.params["rxCS.E230.Tsys1"] = [t, 0]
            self.params["rxCS.E230.Tsys2"] = [t, 0]
            return

        par = "rxCS.E230.Tsys1"
        if par in data:
            tstr = data[par]
            tstr = datetime.strptime(tstr, "%Y-%m-%dT%H:%M:%S.%f").strftime("%s")
            t = int(tstr)
        else:
            t = utime

        if1 = data.split("\n")[0].split()[1]
        tsys1 = data.split("\n")[0].split()[4]
        if2 = data.split("\n")[1].split()[1]
        tsys2 = data.split("\n")[1].split()[4]

        self.params[par] = [t, float(tsys1)]
        par = "rxCS.E230.Tsys2"
        self.params[par] = [t, float(tsys2)]

        print("Debug: ********")
        print(if1, tsys1, if2, tsys2)
        print("Debug: ********")


    def insertRXlock(self, utime, data):
        if data == '':
            t = utime
            self.params["rxCS.E230.isLocked"] = [t, False]
            return

        t = utime

        E230isLocked = False
        try: # fetch "isLocked" field
            for bandinfo in data.split("band"):
                if bandinfo[0] == '3': # Info of Band3 = E230
                    val = bandinfo.split("isLocked")[1].split()[1].strip(",")
                    E230isLocked = val.lower() == "true"
                else: continue
        except Exception as e: # "isLocked" not available, fetching "gunn" and "loop" instead
            print(e)
            print("isLocked value not available from superviseReceiver. Fetch guun and loop fields instead.")
            for bandinfo in data.split("band"):
                if bandinfo[0] == '3': # Info of Band3 = E230
                    E230gunn = int(data.split("band")[2].split("'gunn':")[1].strip(",")[1])
                    E230loop = int(data.split("band")[2].split("'loop':")[1].strip(",")[1])
                    if E230gunn == 1 and E230loop == 1: E230isLocked = True
                else: continue

        self.params["rxCS.E230.isLocked"] = [t, E230isLocked]

# This is a VLBImonitor client.  This client is released under the terms of the MIT Licence.
# Copyright (c) 2016 Radboud RadioLab.  All rights reserved
from __future__ import print_function

import time

#-- limit significant figures
def signif(n, x):
    return float("%.*g" % (n, x))

class Getter():
    def __init__(self, sitelist):
        self.params = {}
        self.sitelist = sitelist

    def updateValues(self):
        self.params.clear()

        #-- default timestamp: now
        #-- time: seconds since 1970 Jan 1 UTC, also known as "unix time".
        utime = time.mktime(time.localtime())

        #-- insert any new values with times at which those values were measured
        #-- if no measurement time is available, use the current time
        self.params['TEL.SOURCE_NAME'] = [utime, 'source']
        self.params['ENVIRO.Air_Temp'] = [utime, signif(4, 15.1 + 273.15)] #-- convert C to K
        self.params['ENVIRO.Pressure'] = [utime, signif(4, .1 * 505.3)] #-- convert hPa to kPa
        self.params['ENVIRO.Humidity'] = [utime, 26.1]
        self.params['ENVIRO.Wind_Speed'] = [utime, 4.2]
        self.params['ENVIRO.JCMT_TAU'] = [utime, 0.4]
        self.params['ENVIRO.rain_event'] = [utime, 'rain']
        self.params['RXA_ENG_TASK.LO_FREQ_DEMAND'] = [utime, 259.05]

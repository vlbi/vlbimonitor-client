from astropy.coordinates.angles import Angle
from spt3g import core, gcp
import os
import glob
import datetime as dt
import numpy as np
import subprocess as sp
import threading
import logging

try:
    # assume module code
    from .iMaser import get_maser_data
except ImportError:
    # for standalone command-line testing
    from iMaser import get_maser_data


deg = core.G3Units.deg
rahr = core.G3Units.rahour
sec = core.G3Units.second


__all__ = ["Sitedata"]


def signif(n, x):
    """Print float to specified number of significant figures"""
    return float("%.*g" % (n, x))


def run_gcp(parent):

    # grab frames from GCP
    pipe = core.G3Pipeline()

    try:
        # try again later if GCP is down
        pipe.Add(gcp.ARCFileReader, filename="tcp://control4.spt:5482")
    except RuntimeError as e:
        return

    class SkipFrames(object):
        def __init__(self, parent):
            self.parent = parent
            self.last = None
            self.stop = False

        def __call__(self, frame):
            if self.stop:
                return False

            if self.parent.stop.is_set():
                self.stop = True
                pipe.halt_processing()
                return False

            if frame.type != core.G3FrameType.GcpSlow:
                return

            now = core.G3Time.Now().time / sec
            if self.last is not None and (now - self.last) < self.parent.gcp_cadence:
                return False

            self.last = now

    pipe.Add(SkipFrames, parent=parent)
    pipe.Add(gcp.ARCExtractor.ARCExtract)

    class GetARCFrame(object):
        def __init__(self, parent):
            self.parent = parent

        def __call__(self, fr):
            if fr.type == core.G3FrameType.GcpSlow:
                with self.parent.lock:
                    del self.parent.frame
                    self.parent.frame = fr
                return False

    pipe.Add(GetARCFrame, parent=parent)
    pipe.Run(signal_halt=False)


class Sitedata(object):
    def __init__(self, sitelist=None, gcp_cadence=10, vlbi_cadence=60):
        self.sitelist = sitelist
        self.gcp_cadence = gcp_cadence
        self.vlbi_cadence = vlbi_cadence
        self.frame = None

        # start GCP thread
        self.stop = threading.Event()
        self.lock = threading.Lock()
        self.thread = None

        # initialize parameters
        self.params = {}
        self.start = int(core.G3Time.Now().time / sec)
        self.vlbi_last = None

        # IF data
        names = ["pol0_usb", "pol0_lsb", "pol1_usb", "pol1_lsb"]
        freqs = [228.1, 214.1, 228.1, 214.1]
        bws = [4.0, 4.0, 4.0, 4.0]
        pols = ["L", "L", "R", "R"]

        for idx, (name, freq, bw, pol) in enumerate(
            zip(names, freqs, bws, pols)
        ):
            tag = "if_{}_".format(idx + 1)
            self.params[tag + "identifier"] = [self.start, name]
            self.params[tag + "centreFreq"] = [self.start, signif(4, freq)]
            self.params[tag + "bandWidth"] = [self.start, signif(4, bw)]
            self.params[tag + "polType"] = [self.start, pol]

        # receiver data
        for k in range(1, 5):
            self.params["rx_{}_loFreq".format(k)] = [self.start, 221.1 if k == 1 else 0]
            self.params["rx_{}_qwpInserted".format(k)] = [
                self.start,
                True if k == 1 else False,
            ]
            self.params["rx_{}_nrIFs".format(k)] = [self.start, 4 if k == 1 else 0]
            if k > 1:
                self.params["rx_{}_online".format(k)] = [self.start, False]
                self.params["rx_{}_lockStatus".format(k)] = [self.start, False]

    def close(self, *args, **kwargs):
        """
        Halt GCP pipeline and close the thread
        """
        if self.thread is not None and self.thread.is_alive():
            logging.debug("Stopping GCP thread")
            self.stop.set()
            self.thread.join()
            self.thread = None
            self.stop.clear()

    def update_gcp_params(self):
        """
        Update GCP parameters from frame
        """
        if self.gcp_cadence is None or self.gcp_cadence < 0:
            return

        # ensure that the GCP acquisition thread is running
        if self.thread is not None and not self.thread.is_alive():
            logging.warning("Found dead GCP thread, restarting")
            self.thread.join()
            self.thread = None
            self.stop.clear()

        if self.thread is None:
            logging.info("Starting GCP thread")
            self.thread = threading.Thread(target=run_gcp, args=(self,))
            self.thread.start()

        # ensure that the GCP acquisition thread is running
        if self.thread is not None and not self.thread.is_alive():
            logging.warning("Found dead GCP thread, restarting")
            self.thread.join()
            self.thread = None
            self.stop.clear()

        if self.thread is None:
            logging.info("Starting GCP thread")
            self.thread = threading.Thread(target=run_gcp, args=(self,))
            self.thread.start()

        # grab the running GCP frame for processing
        with self.lock:
            if self.frame is None:
                return
            logging.debug("Got new GCP frame")
            frame = self.frame
            del self.frame
            self.frame = None

        # telescope data
        tstat = frame["TrackerStatus"]
        stime = tstat.time[-1].time / sec

        tpoint = frame["TrackerPointing"]
        ptime = tpoint.time[-1].time / sec
        pmjd = tpoint.time[-1].mjd

        source = frame["SourceName"]
        on_source = not bool(tstat.source_acquired[-1])
        mode = "idle" if source == "current" else "tracking" if on_source else "slewing"
        self.params["telescope_sourceName"] = [stime, source]
        self.params["telescope_onSource"] = [stime, on_source]
        self.params["telescope_observingMode"] = [stime, mode]

        ra = tpoint.equat_geoc_ra[-1] / rahr
        dec = tpoint.equat_geoc_dec[-1] / deg
        coordstr = "{:.6f}{:+.6f}".format(ra, dec)
        self.params["telescope_raDecTargetCurrent"] = [ptime, coordstr]

        # catalog source position
        try:
            source_pos = sp.check_output(
                "grep \"{}\" $GCP_DIR/config/ephem/source.cat".format(source), shell=True
            ).decode()
            source_pos = source_pos.split("#")[0].split()
            ra = Angle(source_pos[2], "hour").to_value()
            dec = Angle(source_pos[3], "deg").to_value()
            coordstr = "{:.6f}{:+.6f}".format(ra, dec)
            self.params["telescope_raDecTargetJ2000"] = [stime, coordstr]
        except:
            pass

        # pointing
        az = tstat.az_pos[-1]
        el = tstat.el_pos[-1]
        self.params["telescope_azimuthElevation"] = [
            ptime,
            "{:.6f}{:+.6f}".format(az / deg, el / deg),
        ]

        # pointing model correction
        from spt3g.pointing.offline_pointing import CorrectedAz, CorrectedEl

        pmodel = frame["OnlinePointingModel"]
        a0, a1 = pmodel["flexure"][:2]
        a2, a3, a4 = pmodel["tilts"][:3]
        a5, a6 = pmodel["fixedCollimation"][:2]
        az0 = tpoint.encoder_off_x[0]
        refraction = np.median(tpoint.refraction)
        az_corr = CorrectedAz(az, el, a2, a3, a4, a5, az0)
        el_corr = CorrectedEl(az, el, a0, a1, a2, a3, a6, refraction)

        # pointing in celestial coordinates
        from spt3g.maps.azel import convert_azel_to_radec, iers

        ra, dec = convert_azel_to_radec(az_corr, el_corr, mjd=pmjd)
        self.params["telescope_raDecActualCurrent"] = [
            ptime,
            "{:.6f}{:+.6f}".format(ra / rahr, dec / deg),
        ]
        self.params["telescope_apparentRaDec"] = self.params["telescope_raDecActualCurrent"]

        # sky offsets
        xoff = tpoint.sky_off_x[-1] / deg
        xoff = Angle(xoff, "deg").to_string(sep=":", precision=2)
        yoff = tpoint.sky_off_y[-1] / deg
        yoff = Angle(yoff, "deg").to_string(sep=":", precision=2)
        offsets = "sky_offset x={}, y={}".format(xoff, yoff)
        self.params["telescope_pointingCorrection"] = [ptime, offsets]

        # focus
        btime = frame["BenchInfo"]["benchFocus"].start.time / sec
        focus = frame["BenchInfo"]["benchFocus"][-1]
        focusstr = "benchFocus {}".format(signif(4, focus))
        self.params["telescope_focusCorrection"] = [btime, focusstr]

        # spectrometer data
        spec = frame["array"]["spectrometer"]
        spectime = spec["utc"][-1].time / sec
        tps = spec["tp"][-1] + spec["tp_base"][-1]
        if spectime < 0:
            online = False
            # get a sensible timestamp
            spectime = self.params.get("rx_1_online", [self.start])[0]
        else:
            now = core.G3Time.Now().time / sec
            online = bool(np.abs(now - spectime) * sec < core.G3Units.day)
        self.params["rx_1_online"] = [spectime, online]
        for idx, tp in enumerate(tps):
            tag = "if_{}_".format(idx + 1)
            self.params[tag + "online"] = [spectime, online]
            if online:
                self.params[tag + "totalPower"] = [spectime, signif(6, tp)]

        # weather data
        weather = frame["Weather"]
        wtime = frame["WeatherTime"].time / sec
        if wtime > 0:
            temp = signif(4, weather["telescope_temp"] / core.G3Units.K)
            self.params["weather_temperature"] = [wtime, temp]
            pressure = signif(4, weather["telescope_pressure"] / core.G3Units.kPa)
            self.params["weather_airPressure"] = [wtime, pressure]
            self.params["weather_relativeHumidity"] = [wtime, signif(4, weather["rel_humidity"])]
            wind_speed = signif(4, weather["wind_speed"] * sec / core.G3Units.m)
            self.params["weather_windSpeed"] = [wtime, wind_speed]
            wind_dir = signif(4, weather["wind_direction"] / deg)
            self.params["weather_windDirection"] = [wtime, wind_dir]
            # is this ever anything else?
            self.params["weather_precipCondition"] = [wtime, "dry"]
        # convert tipper tau at 350um to tau at 225GHz
        ttime = frame["array"]["tipper"]["utc"].time / sec
        if ttime > 0:
            tau = (weather["tau"] + 0.2) / 29.0
            # convert tau at 350um to PWV
            # Radford & Peterson 2016 prediction for PWV, based on Pardo ATM models
            pwv = 0.46 * weather["tau"] - 0.2
            self.params["weather_tau225"] = [ttime, signif(6, tau)]
            self.params["weather_pwv"] = [ttime, signif(6, pwv)]

    def update_vlbi_params(self):
        """
        Update VLBI receiver parameters
        """
        if self.vlbi_cadence is None or self.vlbi_cadence < 0:
            return

        now = int(core.G3Time.Now().time / sec)
        if self.vlbi_last is not None and (now - self.vlbi_last) < self.vlbi_cadence:
            return

        self.vlbi_last = now

        # system temperature
        log_path = "/home/sptdaq/eht/observation/r2dbe/*.yfac"
        yfac_log = sorted(glob.glob(log_path))[-1]
        with open(yfac_log, "r") as f:
            data = [x.strip().split() for x in f.readlines() if x.strip()][-4:]

        def parse_yfac_data(x):
            time = dt.datetime.strptime(" ".join(x[:2]), "%Y/%m/%d %H:%M:%S")
            time = core.G3Time(time.isoformat()).time / sec
            temp = float(x[2])
            y0 = float(x[3])
            y1 = float(x[4])
            return (
                x[-1],
                {
                    "time": time,
                    "temp": temp,
                    "tsys0": temp / (y0 - 1.0),
                    "tsys1": temp / (y1 - 1.0),
                },
            )

        data = dict(parse_yfac_data(x) for x in data)

        def get_tsys(k, pol):
            t = data[k]["time"]
            d = data[k]["tsys{}".format(pol)]
            return [t, signif(5, d)]

        self.params["if_1_systemTemp"] = get_tsys("r2dbe3", 0)
        self.params["if_2_systemTemp"] = get_tsys("r2dbe1", 0)
        self.params["if_3_systemTemp"] = get_tsys("r2dbe3", 1)
        self.params["if_4_systemTemp"] = get_tsys("r2dbe1", 1)

        # self.params["if_{}_systemTempAzEl"]

        # receiver data
        # self.params["rx_1_lockStatus"]
        self.params["rx_1_tempHot"] = [
            data["r2dbe1"]["time"],
            data["r2dbe1"]["temp"],
        ]

        # maser data
        maser_data = get_maser_data()
        for k, v in maser_data.items():
            if isinstance(v[1], float):
                v[1] = signif(6, float(v[1]))
            self.params[k] = v

        # self.params["maser_timeToGpsTick"]
        # self.params["maser_clockEarlyData"]

    def collect(self):
        """
        Update all the parameters.
        """

        # grab a frame of data from GCP
        self.update_gcp_params()

        # update receiver data
        self.update_vlbi_params()

        # sorted lists of pairs
        params = {}
        for k, v in sorted(self.params.items()):
            params[k] = [[int(v[0]), v[1]]]

        return params


if __name__ == "__main__":

    import argparse as ap
    import time

    P = ap.ArgumentParser()
    P.add_argument(
        "-m", "--map", action="store_true", help="Generate parameter map file"
    )
    args = P.parse_args()

    S = Sitedata()
    params = S.collect()
    while "telescope_sourceName" not in params:
        time.sleep(1)
        params = S.collect()
    S.close()

    if args.map:
        # dummy parameter map, since we use the masterlist parameter names
        with open("SPT.map", "w") as f:
            for k in params:
                f.write("{0} {0}\n".format(k))

    for k, v in params.items():
        print("{}: {}".format(repr(k), repr(v)))

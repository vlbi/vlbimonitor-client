# This is a VLBImonitor client.  This client is released under the terms of the MIT Licence.
# Copyright (c) 2016 Radboud RadioLab.  All rights reserved
from __future__ import print_function

#-- standard libraries
import logging
import os.path

#-- local modules
from .config import rootdir

class PrivacyModeSwitch():
    """Determine the data submission cadence mode.

    The primary use case is to switch between a private mode in which
    sensitive parameters (e.g. source coordinates) are reported and a
    public mode in which only non-sensitive parameters are reported."""

    FILENAME = rootdir + '/privacy_mode'

    def __init__(self, modes):
        self.modes = modes

        #-- init
        self.mode = list(modes)[0]
        self.mtime = -1
        self.err = None

        self.__read()
        logging.info('Privacy mode read: %s' % self.mode)


    def __read(self):
        #-- read mtime
        mtime = os.path.getmtime(self.FILENAME)

        #-- mtime did not change
        if mtime == self.mtime:
            return

        #-- mtime did change, read file
        with open(self.FILENAME, 'r') as f:
            mode = f.readline().strip()

        #-- unknown mode, ignore
        if not mode in self.modes:
            raise Exception('Privacy mode read is unknown (%s), keeping current mode (%s)' % (mode, self.mode))

        self.mode = mode
        self.mtime = mtime
        self.err = None


    def get(self):
        oldmode = self.mode
        try:
            self.__read()
        except Exception as e:
            #-- log exception once
            if type(e) != type(self.err)  or  e.args != self.err.args:
                logging.error(e)
                self.err = e

        #-- log changes
        if self.mode != oldmode:
            logging.info('Privacy mode switch: %s' % self.mode)

        return self.mode

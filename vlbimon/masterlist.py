# This is a VLBImonitor client.  This client is released under the terms of the MIT Licence.
# Copyright (c) 2016 Radboud RadioLab.  All rights reserved
from __future__ import print_function

#-- standard libraries
import os
import collections
import logging

class Masterlist:
    COLNAMES = [
        'parname',
        'cadence',
        'partype',
        'datatype',
        'units',
        'valueFunction',
        'validFunction',
        'oldnames',
        'description']

    class BadMasterlist(Exception): pass

    def __init__(self, fname):
        self.fname = fname

    def read(self):
        """Read the masterlist"""
#{{{
#-- only read again if file has been modified
        mtime = os.stat(self.fname).st_mtime
        try:
            if mtime == self.mtime: return
        except AttributeError:
            pass
        self.mtime = mtime

        logging.info("reading: " + self.fname)
        with open(self.fname, 'r') as f:
            lines = f.readlines()

        newparams = collections.OrderedDict()
        allnames = set()
        errors = []
        for iline, line in enumerate(lines):
            line = line.strip()
            #-- skip comment and empty lines
            if len(line) == 0: continue
            if line[0] == "#": continue

            fields = line.split('\t')
            par = fields[0]
#-- verify lines
            #-- incomplete line
            if len(fields) < self.COLNAMES.index('datatype')+1:  #-- datatype is required
                msg = "masterlist(%d): too few fields"
                errors.append(msg % (iline+1))
                continue
            #-- line is too long
            if len(fields) > len(self.COLNAMES):
                msg = "masterlist(%d): too many fields"
                errors.append(msg % (iline+1))
                continue
            #-- missing parname
            if len(par) == 0:
                msg = "masterlist(%d): missing parname"
                errors.append(msg % (iline+1))
                continue
            #-- duplicate parname
            if par in newparams:
                msg = "masterlist(%d): duplicate parameter"
                errors.append(msg % (iline+1))
                continue

#-- put fields in a dict
            d = collections.OrderedDict()
            for i, field in enumerate(fields):
                if i == 0: continue  #-- skip parname
                if len(field) == 0: continue

                #-- stray spaces
                if field[0] == ' '  or  field[-1] == ' '  or \
                      "  " in field:
                    msg = "masterlist(%d): stray spaces"
                    errors.append(msg % (iline+1))

                key = self.COLNAMES[i]

                #-- cadence is a dict with number values
                if key in ['cadence']:
                    d_ = {}
                    for word in field.split(','):
                        k, v = word.strip().split(':')
                        d_[k] = float(v)
                    field = d_

                #-- split oldnames
                if key in ['oldnames']: field = field.split(',')

                d[key] = field

            #-- verify unique oldnames
            allnames.add(par)
            if 'oldnames' in d:
                oldnames = set(d['oldnames'])
                duplicates = allnames.intersection(oldnames)
                allnames = allnames.union(oldnames)
                if len(duplicates) > 0:
                    msg = "masterlist(%d): duplicate Former Names: %s"
                    errors.append(msg % (iline+1, list(duplicates)))
                    continue

            newparams[par] = d


        #-- die if errors encountered
        if len(errors) != 0:
            msg = "\n".join(errors)
            raise self.BadMasterlist(msg)

#-- save if no errors
        logging.warning("masterlist read successfully")
        return newparams
#}}}

# vim: foldmethod=marker
